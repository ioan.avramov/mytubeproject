﻿using MyTube.Entities;
using MyTube.Repositories;
using MyTube.ViewModels.HomeModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyTube.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginVM model)
        {
            if (ModelState.IsValid)
            {
                UserRepository repo = new UserRepository();
                User loggedUser = repo.GetFirstOrDefault(u =>
                                            u.Username == model.Username &&
                                            u.Password == model.Password);

                if (loggedUser == null)
                    ModelState.AddModelError("AuthError", "Invalid username and password!");
                else
                    Session["loggedUser"] = loggedUser;
            }

            if (!ModelState.IsValid)
                return View(model);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logout()
        {
            Session["loggedUser"] = null;

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Register()
        {
            if (!(Session["loggedUser"] == null))
                return RedirectToAction("Index", "Home");

            return View();
        }
        [HttpPost]
        public ActionResult Register(RegisterVM model)
        {
            if (ModelState.IsValid)
            {
                UserRepository repo = new UserRepository();
                PlayListRepository listRepo = new PlayListRepository();
                User exist = repo.GetFirstOrDefault(u => u.Username == model.Username);

                if (!(exist == null))
                {
                    ModelState.AddModelError("RegError", "This username is already taken.");
                }
                else
                {
                    User created = new User
                    {
                        Username = model.Username,
                        Password = model.Password,
                        FirstName = model.FirstName,
                        LastName = model.LastName
                    };
                    repo.Insert(created);
                    Session["loggedUser"] = created;

                    PlayList list = new PlayList
                    {
                        Name = created.Username,
                        OwnerId = ((User)Session["loggedUser"]).Id,
                    };
                    listRepo.Insert(list);
                }
            }
            if (!ModelState.IsValid)
                return View(model);

            return RedirectToAction("Index", "Home");


        }

    }
}