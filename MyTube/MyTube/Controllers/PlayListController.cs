﻿using MyTube.Entities;
using MyTube.Repositories;
using MyTube.ViewModels.PlayListModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyTube.Controllers
{
    public class PlayListController : Controller   
    {
        
        public ActionResult Index(IndexVM model)
        {
            if (Session["loggedUser"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            User loggedUser = (User)Session["loggedUser"];

            model.Page = model.Page <= 0 ? 1 : model.Page;

            model.ItemsPerPage = model.ItemsPerPage <= 0 ? 10 : model.ItemsPerPage;

            PlayListRepository plRepo = new PlayListRepository();
            PlayList playL = plRepo.GetFirstOrDefault(i => i.OwnerId == loggedUser.Id);

            VideoToPlayListRepository vtpRepo = new VideoToPlayListRepository();

            model.Items = vtpRepo.GetAllDto(i => i.Video, i => i.PlayListId == playL.Id, model.Page, model.ItemsPerPage);

            model.PagesCount = (int)Math.Ceiling(vtpRepo.Count(i => i.PlayListId == playL.Id) / (double)model.ItemsPerPage);

            return View(model);
        }
        public ActionResult Add(AddVM model)
        {
            if (Session["loggedUser"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            User user = (User)Session["loggedUser"];

            PlayListRepository pLRepo = new PlayListRepository();
            PlayList pList = pLRepo.GetFirstOrDefault(i => i.OwnerId == user.Id);

            VideoRepository vRepo = new VideoRepository();
            Video video = vRepo.GetFirstOrDefault(i => i.Id == model.VideoId);

            VideoToPlayList addToPlayList = new VideoToPlayList
            {
                VideoId = video.Id,
                PlayListId = pList.Id
            };

            VideoToPlayListRepository vTPRepo = new VideoToPlayListRepository();
            vTPRepo.Insert(addToPlayList);

            return RedirectToAction("Index", "PlayList");
        }
        public ActionResult Delete(int VideoId)
        {
            if (Session["loggedUser"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            User user = (User)Session["loggedUser"];

            PlayListRepository pLRepo = new PlayListRepository();
            PlayList pList = pLRepo.GetFirstOrDefault(i => i.OwnerId == user.Id);

            VideoToPlayListRepository vTPLRepo = new VideoToPlayListRepository();

            vTPLRepo.Delete(vTPLRepo.GetFirstOrDefault(i => i.PlayListId == pList.Id && i.VideoId == VideoId).Id);

            return RedirectToAction("Index", "PlayList");
        }
    }
}