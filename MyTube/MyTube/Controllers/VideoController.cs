﻿using MyTube.Entities;
using MyTube.Repositories;
using MyTube.ViewModels.VideoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyTube.Controllers
{
    public class VideoController : Controller
    {
        public ActionResult Index(IndexVM model)
        {
            if (Session["loggedUser"] == null)
                return RedirectToAction("Login", "Home");

            User loggedUser = (User)Session["loggedUser"];

            model.Page = model.Page <= 0
                                ? 1
                                : model.Page;

            model.ItemsPerPage = model.ItemsPerPage <= 0
                                            ? 10
                                            : model.ItemsPerPage;

            VideoRepository listRepo = new VideoRepository();

            model.Items = listRepo.GetAll(
                                    i => true,
                                    model.Page,
                                    model.ItemsPerPage);

            model.PagesCount = (int)Math.Ceiling(listRepo.Count() / (double)model.ItemsPerPage );

            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            if (Session["loggedUser"] == null)
                return RedirectToAction("Login", "Home");

            return View();
        }
        
        [HttpPost]
        public ActionResult Add(CreateVM model)
        {
            if (ModelState.IsValid)
            {
                VideoRepository repo = new VideoRepository();
                Video exist = repo.GetFirstOrDefault(u => u.Link == model.Link);

                if (!(exist == null))
                {
                    ModelState.AddModelError("VideoCreateError", "This video already exists");
                }
                else
                {
                    model.Link = model.Link.Substring(model.Link.LastIndexOf('=') + 1);
                    Video created = new Video
                    {
                        CreatorId = ((User)Session["loggedUser"]).Id,
                        Title = model.Title,
                        Link = "https://www.youtube.com/embed/"+model.Link

                };
                    repo.Insert(created);
                }
            }
            if (!ModelState.IsValid)
                return View(model);

            return RedirectToAction("Index", "Video");
        }
        public ActionResult Delete(int VideoId)
        {
            if (Session["loggedUser"] == null)
                return RedirectToAction("Login", "Home");


            VideoToPlayListRepository vtplRepo = new VideoToPlayListRepository();

            while (vtplRepo.GetFirstOrDefault(i=> i.VideoId == VideoId) != null)
            {
                vtplRepo.Delete(vtplRepo.GetFirstOrDefault(i => i.VideoId == VideoId).Id);
            }

            VideoRepository repo = new VideoRepository();
            repo.Delete(VideoId);


            return RedirectToAction("Index", "Video");
        }
    }
}