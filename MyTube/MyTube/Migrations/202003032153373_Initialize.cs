﻿namespace MyTube.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initialize : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PlayLists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        OwnerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.OwnerId, cascadeDelete: true)
                .Index(t => t.OwnerId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Videos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreatorId = c.Int(nullable: false),
                        Title = c.String(),
                        Link = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatorId, cascadeDelete: true)
                .Index(t => t.CreatorId);
            
            CreateTable(
                "dbo.VideoToPlayLists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PlayListId = c.Int(nullable: false),
                        VideoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PlayLists", t => t.PlayListId, cascadeDelete: true)
                .ForeignKey("dbo.Videos", t => t.VideoId, cascadeDelete: false)
                .Index(t => t.PlayListId)
                .Index(t => t.VideoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VideoToPlayLists", "VideoId", "dbo.Videos");
            DropForeignKey("dbo.VideoToPlayLists", "PlayListId", "dbo.PlayLists");
            DropForeignKey("dbo.Videos", "CreatorId", "dbo.Users");
            DropForeignKey("dbo.PlayLists", "OwnerId", "dbo.Users");
            DropIndex("dbo.VideoToPlayLists", new[] { "VideoId" });
            DropIndex("dbo.VideoToPlayLists", new[] { "PlayListId" });
            DropIndex("dbo.Videos", new[] { "CreatorId" });
            DropIndex("dbo.PlayLists", new[] { "OwnerId" });
            DropTable("dbo.VideoToPlayLists");
            DropTable("dbo.Videos");
            DropTable("dbo.Users");
            DropTable("dbo.PlayLists");
        }
    }
}
