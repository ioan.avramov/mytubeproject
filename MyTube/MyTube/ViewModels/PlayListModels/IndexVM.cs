﻿using MyTube.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyTube.ViewModels.PlayListModels
{
    public class IndexVM
    {
        public List<Video> Items { get; set; } 
        public int Page { get; set; }
        public int ItemsPerPage { get; set; }
        public int PagesCount { get; set; }
    }
}