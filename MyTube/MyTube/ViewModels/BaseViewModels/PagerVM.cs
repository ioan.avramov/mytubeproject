﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyTube.ViewModels.BaseViewModels
{
    public class PagerVM
    {
        public int Page { get; set; }
        public int ItemsPerPage { get; set; }
        public int PagesCount { get; set; }
    }
}