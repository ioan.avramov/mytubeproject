﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyTube.ViewModels.HomeModels
{
    public class LoginVM
    {
        [DisplayName("Username: ")]
        [Required(ErrorMessage ="<= Do you see anyhting here??!")]
        public string Username { get; set; }

        [DisplayName("Password: ")]
        [Required(ErrorMessage = "<= Do you see anyhting here??!")]
        public string Password { get; set; }

    }
}