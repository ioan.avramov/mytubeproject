﻿using MyTube.Entities;
using System.Collections.Generic;

namespace MyTube.ViewModels.VideoModels
{
    public class IndexVM
    {
        public List<Video> Items { get; set; }
        public int Page { get; set; }
        public int ItemsPerPage { get; set; }
        public int PagesCount { get; set; }
    }
}