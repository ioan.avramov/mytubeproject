﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyTube.ViewModels.VideoModels
{
    public class CreateVM
    {
        [DisplayName("Title: ")]
        [Required(ErrorMessage ="Add a God damn title...")]
        public string Title { get; set; }

        [DisplayName("Link: ")]
        [Required(ErrorMessage = "Add a God damn link...")]
        public string Link { get; set; }
    }
}