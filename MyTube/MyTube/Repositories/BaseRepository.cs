﻿using MyTube.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace MyTube.Repositories
{
    public class BaseRepository<T>
        where T : BaseEntity
    {
        protected DbContext Context { get; set; }
        protected DbSet<T> Items { get; set; }

        public BaseRepository()
        {
            Context = new MyTubeDbContext();
            Items = Context.Set<T>();
        }

        public List<T> GetAll(Expression<Func<T, bool>> filter = null,
                              int page = 1,
                              int itemsPerPage = int.MaxValue)
        {
            IQueryable<T> query = Items;

            if (filter != null)
                query = query.Where(filter);

            return query
                    .OrderBy(i => i.Id)
                    .Skip((page - 1) * itemsPerPage)
                    .Take(itemsPerPage)
                    .ToList();
        }


        public List<E> GetAllDto<E>(Expression<Func<T, E>> select,
                                    Expression<Func<T, bool>> filter = null,
                                    int page = 1,
                                    int itemsPerPage = int.MaxValue)
        {
            IQueryable<T> query = Items;

            if (filter != null)
                query = query.Where(filter);

            query = query
                        .OrderBy(i => i.Id)
                        .Skip((page - 1) * itemsPerPage)
                        .Take(itemsPerPage);

            return query
                    .Select(select)
                    .ToList();
        }

        public T GetById(int id)
        {
            return Items
                    .Where(i => i.Id == id)
                    .FirstOrDefault();
        }

        public T GetFirstOrDefault(Expression<Func<T, bool>> filter)
        {
            return Items
                      .Where(filter)
                      .FirstOrDefault();
        }

        public int Count(Expression<Func<T, bool>> filter = null)
        {
            IQueryable<T> query = Items;

            if (filter != null)
                query = query.Where(filter);

            return query.Count();
        }

        public void Insert(T item)
        {
            Items.Add(item);
            Context.SaveChanges();
        }

        public void Update(T item)
        {
            DbEntityEntry entry = Context.Entry(item);
            entry.State = EntityState.Modified;

            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            T item = GetById(id);

            if (item != null)
            {
                Items.Remove(item);
                Context.SaveChanges();
            }
        }
    }
}