﻿using MyTube.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MyTube.Repositories
{
    public class MyTubeDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<PlayList> PlayLists { get; set; }
        public DbSet<Video> Videos { get; set; }
        public DbSet<VideoToPlayList> VideoToPlayLists { get; set; }

        public MyTubeDbContext()
            : base("Server=localhost;Database=MyTubeDb;User Id=ioandb;Password=ioandb;")
        {
            Users = this.Set<User>();
            PlayLists = this.Set<PlayList>();
            Videos = this.Set<Video>();
            VideoToPlayLists = this.Set<VideoToPlayList>();
        }
    }
}