﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace MyTube.Entities
{
    public class PlayList : BaseEntity
    {
        public string Name  { get; set; }
        public int OwnerId { get; set; }

        [ForeignKey("OwnerId")]
        public User User { get; set; }
    }
}