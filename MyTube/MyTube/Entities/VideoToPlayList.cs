﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyTube.Entities
{
    public class VideoToPlayList : BaseEntity
    {
        public int PlayListId { get; set; }
        public int VideoId { get; set; }


        [ForeignKey("PlayListId")]
        public PlayList PlayList { get; set; }

        [ForeignKey("VideoId")]
        public Video Video { get; set; }

    }
}