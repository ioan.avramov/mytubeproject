﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyTube.Entities
{
    public class Video : BaseEntity
    {
        public int CreatorId { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }

        [ForeignKey("CreatorId")]
        public User User { get; set; }
    }
}